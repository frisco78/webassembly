// Wasm

const WasmLoader = class {
    constructor(path, refFunc) {
        this.path = path
        this.refFunc = refFunc
    }

    async importWasmInstance() {
        const res = await fetch(this.path);
        const rawBytes = await res.arrayBuffer();
        const module = new WebAssembly.Module(rawBytes);
        const instance = new WebAssembly.Instance(module);

        return instance;
    }

    async initFunctions() {
        const instance = await this.importWasmInstance();

        let functions = {};

        Object.keys(instance.exports).forEach(func => {
            this.refFunc.forEach(keyFunc => {
                if (keyFunc == func) {
                    functions = { ...functions, [keyFunc]: instance.exports[func] };
                };
            })
        })

        return functions;
    }

    async run(method, a, b) {
        const instance = await this.initFunctions();
        let result = 'Error';
        switch (method) {
            case '+':
                result = instance.add(a, b);
                break;
            case '-':
                result = instance.substract(a, b);
                break;
            case 'x':
                result = instance.multiply(a, b);
                break;
            case '/':
                result = instance.divide(a, b);
                break;
            default:
                break;
        }
        return result
    }
};

const wasmLoader = new WasmLoader('./math.wasm', ['add', 'substract', 'multiply', 'divide']);

// execute calculus
const calculate = (expression) => {
    const re = /([0-9]*)([+-x\/]{1})([0-9]*)/;
    const groups = re.exec(expression)
    console.log(groups)
    const a = groups[1];
    const b = groups[3];
    // [+, -, x, /] are supported
    const method = groups[2];

    return wasmLoader.run(method, a, b);
}

// interact with Frontend

const calc = document.getElementById('calc');
const result = document.getElementById('res');

const buttons = document.getElementById('buttons');
buttons.addEventListener('click', async function (event) {
    const targetId = event.target.id;

    if (targetId == '' || targetId == 'buttons') {
        return;
    }

    switch(targetId) {
        // remove last info
        case 'suppr':
            calc.innerHTML = calc.innerHTML.substring(0, calc.innerHTML.length - 1)
            break;
        // delete whole expression
        case 'restart':
            calc.innerHTML = ''
            break;
        // exectute calc
        case '=':
            const resultData = await calculate(calc.innerHTML)
            result.innerHTML = resultData;
            break;
        // update expression with new value    
        default:
            calc.innerHTML += targetId;
            break;
    }
}); 
